import { Component } from '@angular/core';

@Component({
  selector: 'app-demo-si',
  templateUrl: './demo-si.component.html',
  styleUrls: ['./demo-si.component.css']
})
export class DemoSIComponent {
  Name = "somesh";
  time: string = "1pm";
  inputValue: string = '';

  FSAD = {
    name: 'Somesh',
    classroom: 'CS106',
    student: 50,
    classImage: '/assets/classroom.jpg'
  };

  whenchanged(event: any) {
    this.time = event.target.value;
  }

  onButtonClick() {
    alert('Button was clicked!');
  }
  
  onKey(event: any) {
    this.inputValue = event.target.value;
    console.log(this.inputValue);
  }
}
