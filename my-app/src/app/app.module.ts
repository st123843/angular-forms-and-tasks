import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopheaderComponent } from './topheader/topheader.component';
import { DemoSIComponent } from './demo-si/demo-si.component';
import { HoverHighlightDirective } from './directives/hover-highlight.directive';
import { DisableButtonDirective } from './directives/disable-button.directive';
import { LoginRegistrationFormsComponent } from './login-registration-forms/login-registration-forms.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginFormComponent } from './login-form/login-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TopheaderComponent,
    DemoSIComponent,
    HoverHighlightDirective,
    DisableButtonDirective,
    LoginRegistrationFormsComponent,
    LoginFormComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
