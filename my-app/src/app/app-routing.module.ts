import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoSIComponent } from './demo-si/demo-si.component';
import { LoginRegistrationFormsComponent } from './login-registration-forms/login-registration-forms.component';
import { LoginFormComponent } from './login-form/login-form.component';

const routes: Routes = [
  { path: '', component: DemoSIComponent },
  { path: 'login-registration', component: LoginRegistrationFormsComponent },
  { path: 'login', component: LoginFormComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
