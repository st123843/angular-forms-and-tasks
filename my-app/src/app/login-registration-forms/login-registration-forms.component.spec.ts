import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginRegistrationFormsComponent } from './login-registration-forms.component';

describe('LoginRegistrationFormsComponent', () => {
  let component: LoginRegistrationFormsComponent;
  let fixture: ComponentFixture<LoginRegistrationFormsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginRegistrationFormsComponent]
    });
    fixture = TestBed.createComponent(LoginRegistrationFormsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
