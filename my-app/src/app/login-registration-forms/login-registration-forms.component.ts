import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-registration-forms',
  templateUrl: './login-registration-forms.component.html',
  styleUrls: ['./login-registration-forms.component.css']
})
export class LoginRegistrationFormsComponent {

  constructor(private router: Router) {}
  
  onLoginSubmit(form: NgForm) {
    console.log('Login form submitted', form.value);
  }

  onRegSubmit(form: NgForm) {
    console.log('Registration form submitted', form.value);
  }

  passwordsMatch(form: NgForm) {
    return form.value.regPassword === form.value.confirmPassword;
  }

  navigateToHome() {
    this.router.navigate(['/']);
  }

  navigateToLogin() {
    this.router.navigate(['/login']);
  }
}
