NgIf
The DOM element can be conditionally added to or removed using NgIf. It works well for selectively revealing or obscuring things.

<!-- app.component.html -->
<div *ngIf="showMessage">
  Hello, World!
</div>

// app.component.ts
export class AppComponent {
  showMessage = true; // Change this to false to hide the message
}


NgFor
When rendering a list, NgFor repeats a DOM element for each item in an array.

<!-- app.component.html -->
<ul>
  <li *ngFor="let item of items">
    {{ item }}
  </li>
</ul>

// app.component.ts
export class AppComponent {
  items = ['Apple', 'Banana', 'Cherry'];
}


NgSwitch
By comparing an expression to a range of potential values, NgSwitch is used to add and remove DOM elements. It is a method for making intricately nested NgIf simpler.

<!-- app.component.html -->
<div [ngSwitch]="fruit">
  <p *ngSwitchCase="'apple'">You selected Apple.</p>
  <p *ngSwitchCase="'banana'">You selected Banana.</p>
  <p *ngSwitchDefault>Select a fruit!</p>
</div>

// app.component.ts
export class AppComponent {
  fruit = 'apple'; // Change this to see different messages
}